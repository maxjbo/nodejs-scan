package main

import (
	"regexp"
	"strings"
)

// RuleSet is a set of rules.
type RuleSet struct {
	StringRuleDefs []RuleDef `xml:"rule"`
	RegexRuleDefs  []RuleDef `xml:"regex"`
}

// RuleDef is a rule definition.
type RuleDef struct {
	Name        string `xml:"name,attr"`
	Signature   string `xml:"signature"`
	Description string `xml:"description"`
	Tag         string `xml:"tag"`
}

// Rule is implemented by a rule so that it can be evaluated.
type Rule interface {
	Match(string) bool
	Definition() RuleDef
}

// StringRule uses string comparison to establish whether a line contains vulnerable code.
type StringRule RuleDef

// Match tells whether the rule applies to the given line of the source code.
func (r StringRule) Match(s string) bool {
	return strings.Contains(s, r.Signature)
}

// Definition returns the definition of the rule.
func (r StringRule) Definition() RuleDef {
	return RuleDef(r)
}

// NewStringRule creates a new string-based rule.
func NewStringRule(def RuleDef) StringRule {
	return StringRule(def)
}

// RegexRule uses a regexp to establish whether a line contains vulnerable code.
type RegexRule struct {
	RuleDef
	*regexp.Regexp
}

// Match tells whether the rule applies to the given line of the source code.
func (r RegexRule) Match(s string) bool {
	return r.Regexp.MatchString(s)
}

// Definition returns the definition of the rule.
func (r RegexRule) Definition() RuleDef {
	return r.RuleDef
}

// NewRegexRule creates a new regex-based rule.
func NewRegexRule(def RuleDef) (*RegexRule, error) {
	sig := def.Signature
	sig = strings.Replace(sig, "{0,4000}", "{0,100}", -1)  // HACK: work around invalid repeat count
	sig = strings.Replace(sig, "{0,40000}", "{0,100}", -1) // HACK: work around invalid repeat count
	r, err := regexp.Compile(sig)
	if err != nil {
		return nil, err
	}
	return &RegexRule{def, r}, nil
}

// Rules converts +StringRules+ and +RegexRules+ to a slice of +Rule+.
func (set RuleSet) Rules() ([]Rule, error) {
	rules := []Rule{}
	for _, def := range set.StringRuleDefs {
		rules = append(rules, NewStringRule(def))
	}
	for _, def := range set.RegexRuleDefs {
		rule, err := NewRegexRule(def)
		if err != nil {
			return nil, err
		}
		rules = append(rules, rule)
	}
	return rules, nil
}

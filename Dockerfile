FROM  node:9.11.2-alpine

ARG NODEJS_SCAN_VERSION
ARG BABEL_CLI_VERSION

ENV NODEJS_SCAN_VERSION ${NODEJS_SCAN_VERSION:-2.5}
ENV BABEL_CLI_VERSION ${BABEL_CLI_VERSION:-6.26.0}

USER node
WORKDIR /home/node
RUN npm install babel-cli@$BABEL_CLI_VERSION babel-preset-stage-0 babel-preset-react babel-preset-flow --save-dev
ADD --chown=node:node https://raw.githubusercontent.com/ajinabraham/NodeJsScan/v${NODEJS_SCAN_VERSION}/core/rules.xml /home/node/

COPY --chown=root:root analyzer /

ENTRYPOINT []
CMD ["/analyzer", "run"]

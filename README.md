# NodeJsScan analyzer

GitLab Analyzer for NodeJS projects.
It's based on [the rules](https://github.com/ajinabraham/NodeJsScan/blob/master/core/rules.xml)
and signatures of [NodeJsScan](https://github.com/ajinabraham/NodeJsScan).
It only processes `.js` files even though [NodeJsScan](https://github.com/ajinabraham/NodeJsScan) can handle other file types.
Also, this analyzer uses the [Closure Compiler](https://developers.google.com/closure/compiler/)
to remove comments that could otherwise match the signatures of NodeJsScan's rules.

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Versioning and release process

Please check the [Release Process documentation](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/release_process.md).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the GitLab Enterprise Edition (EE) license, see the [LICENSE](LICENSE) file.
